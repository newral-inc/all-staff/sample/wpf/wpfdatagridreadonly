﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFDataGridReadOnly
{
    class Person
    {
        public string Name { get; set; }
        public int age { get; set; }
    }

    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var personList = new List<Person>();
            personList.Add(new Person() { Name = "山田", age = 20 });
            personList.Add(new Person() { Name = "田村", age = 30 });
            personList.Add(new Person() { Name = "佐藤", age = 40 });
            dataGrid.ItemsSource = personList;
        }
    }
}
